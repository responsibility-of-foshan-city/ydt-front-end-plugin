# IP输入框

- author: 杨正
- amend: -



## 平台兼容性

| Vue2 | Vue3 |
| :--: | :--: |
|  ×   |  √   |

| App  | 快应用 | 微信小程序 | 支付宝小程序 | 百度小程序 | 字节小程序 | QQ小程序 |
| :--: | :----: | :--------: | :----------: | :--------: | :--------: | :------: |
|  √   |   ×    |     √      |      ×       |     ×      |     ×      |    ×     |

| 钉钉小程序 | 快手小程序 | 飞书小程序 | 京东小程序 |
| :--------: | :--------: | :--------: | :--------: |
|     ×      |     ×      |     ×      |     ×      |

| H5-Safari | Android Browser | 微信浏览器(Android) | QQ浏览器(Android) | Chrome |  IE  | Edge | Firefox | PC-Safari |
| :-------: | :-------------: | :-----------------: | :---------------: | :----: | :--: | :--: | :-----: | :-------: |
|     √     |        ×        |          ×          |         ×         |   √    |  ×   |  ×   |    √    |     ×     |

<br>

IP输入框，效果如下图所示。

![01](./docData/01.png)

IP输入框支持ip地址的输入，当ip地址子项的内容到达3位时，光标会自动跳入下一个子项。

支持端口输入。

<br>

## 例子

### 使用IP输入框

将 `IpInput` 放入 `components` 目录下，在需要的页面中引入使用。

```html
<template>
  <!-- IP输入框 -->
  <IpInput
  	:ipArray="ipArr"
  	:port="port"
    showPort
  	@change="handleChange"
	></IpInput>
</template>

<script setup>
import { ref } from 'vue'
import IpInput from '@/components/IpInput/IpInput.vue'

// ip地址（数组）
const ipArr = ref(['', '', '', ''])

// 端口号
const prop = ref('')

function handleChange(value) {
  // 获取ip地址（数组）并赋值
	arr.value = value.ip_array
  // 获取端口号并赋值
	port.value = value.port
}
</script>
```

<br>

## 属性

| 属性名      | 说明           | 类型    | 可选值              | 默认值           |
| ----------- | -------------- | ------- | ------------------- | ---------------- |
| ipArray     | ip地址         | Array   | ['', '', '', '']    | ['', '', '', ''] |
| port        | 端口号         | String  | -                   | ''               |
| showPort    | 是否显示端口号 | Boolean | true \| false       | true             |
| height      | 组件高度       | String  | px、rpx等带单位的值 | '60rpx'          |
| border      | 是否显示边框   | Boolean | true \|false        | false            |
| borderColor | 边框颜色       | String  | -                   | '#ccc'           |
| itemBgColor | 子项背景色     | String  | -                   | 'transparent'    |

<br>

## 事件

| 事件名  | 说明                                                         | 参数 | 返回值                                                       |
| ------- | ------------------------------------------------------------ | ---- | ------------------------------------------------------------ |
| @change | 输入时会触发该事件，建议将返回值同步修改到ip数组和端口变量。 | -    | {<br/>		ip_string: ip地址（字符串）,<br/>		ip_array: ip地址（数组）,<br/>		port: 端口<br/>	} |





---

# 待开发

- 删除操作，当删除完其中一个子项的内容，再按一次删除键时，光标跳到上一个子项。

# 日志

- `v1.0.1` 修复无法输入0的问题。2023-05-11

