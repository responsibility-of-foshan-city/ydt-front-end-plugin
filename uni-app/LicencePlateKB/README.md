# 车牌键盘 LicencePlateKB

- author: 杨正
- amend: -



## 平台兼容性

| Vue2 | Vue3 |
| :--: | :--: |
|  ×   |  √   |

| App  | 快应用 | 微信小程序 | 支付宝小程序 | 百度小程序 | 字节小程序 | QQ小程序 |
| :--: | :----: | :--------: | :----------: | :--------: | :--------: | :------: |
|  √   |   -    |     √      |      √       |     -      |     -      |    -     |

| 钉钉小程序 | 快手小程序 | 飞书小程序 | 京东小程序 |
| :--------: | :--------: | :--------: | :--------: |
|     -      |     -      |     -      |     -      |

| H5-Safari | Android Browser | 微信浏览器(Android) | QQ浏览器(Android) | Chrome |  IE  | Edge | Firefox | PC-Safari |
| :-------: | :-------------: | :-----------------: | :---------------: | :----: | :--: | :--: | :-----: | :-------: |
|     -     |        -        |          -          |         -         |   √    |  -   |  -   |    √    |     -     |

<br>

## 组件介绍

车牌键盘，在输入完省份后会自动切换到字母面板。

效果如下图所示。

![01](./docData/01.png)

<br>

## 例子

### 使用车牌键盘

将 `LicencePlateKB` 放入 `components` 目录下，在需要的页面中引入使用。

```html
<template>

  <!-- 车牌键盘 -->
  <LicencePlateKB
    @onInput="oninput"
    @onDelete="onDelete"
    @onHide="onHide"
  />
</template>

<script setup>
import { ref } from 'vue'
import LicencePlateKB from '@/components/LicencePlateKB/LicencePlateKB.vue'

// 输入
function oninput(data) {
  console.log('输入的内容：', data)
}

// 删除
function onDelete() {
  console.log("点击了删除按钮")
}

// 隐藏键盘
function onHide() {
  console.log("点击了隐藏键盘的按钮")
}
</script>
```

<br>

### 暗黑主题

![02](./docData/02.png)

```html
<LicencePlateKB
  theme="dark"
/>
```

<br>

### 修改键盘类型

可以从父组件控制车牌键盘的类型，比如让键盘默认为字母面板。

但用户通过键盘上的面板切换按钮去切换键盘时，键盘的类型还是会被改变。

通过设置车牌键盘的 `kbType` 修改键盘面板类型，该属性接受 `'city'` 和 `'letter'`

```html
<LicencePlateKB
  ref="kbRef"
/>

<script setup>
import { ref } from 'vue'

const kbRef = ref(null)

kbRef.value.kbType = 'letter'
</script>
```

<br>

### 弹窗模式

弹窗模式需要关注以下几个步骤：

1. 将 `popup` 设置为 `true`。
2. 需要给键盘添加一个 `ref` 绑定。
3. 获取键盘的 `display` 属性，判断键盘当前属于 *打开* 还是*关闭状态*。
4. 通过 `open()` 打开键盘。
5. 通过 `close()` 关闭键盘。 

![03](docData/03.gif)

```html
<LicencePlateKB
  ref="licencePlateKBRef"
  popup
/>
<button @click="toggle">打开/关闭键盘</button>

<script setup>
import { ref } from 'vue'

const licencePlateKBRef = ref(null)

function toggle() {
  licencePlateKBRef.value.display ?
    licencePlateKBRef.value.close() :
    licencePlateKBRef.value.open()
}
</script>
```

<br>

上面的代码也可以改成下面这样，**但我不建议，因为我以后可能会在 `open()` 和 `close()` 里加入一些拦截器**。

```js
// 省略部分代码

function toggle() {
  licencePlateKBRef.value.display = !licencePlateKBRef.value.display
}
```

<br>

## 属性

| 属性名 | 说明                                                  | 类型    | 可选值             | 默认值 |
| ------ | ----------------------------------------------------- | ------- | ------------------ | ------ |
| theme   | 键盘主题。 | String | 'light'｜'dark' | 'light' |
| autoToLetter | 在省市模式输入完自动切换到字母模式。 | Boolean | true ｜ false     | true    |
| popup | 弹窗模式。 | Boolean | true ｜ false | false |

<br>

## 事件

| 事件名    | 说明                                                         | 参数 | 返回值                 |
| --------- | ------------------------------------------------------------ | ---- | ---------------------- |
| @onInput  | 输入操作，返回当前输入的字符串。                             | -    | String: 返回输入的内容 |
| @onDelete | 删除操作。                                                   | -    | -                      |
| @onHide   | 隐藏键盘。<br>**由于键盘本身没有隐藏功能，该事件只是向上通知用户点击了隐藏键盘的按钮**。 | -    | -                      |

<br>

## 方法

| 方法名 | 说明                          | 参数 | 返回值                      |
| ------ | ----------------------------- | ---- | --------------------------- |
| open   | 打开键盘。popup为true时有效。 | -    | display，键盘当前展示状态。 |
| close  | 关闭键盘。popup为true时有效。 | -    | display，键盘当前展示状态。 |

<br>

## 对外暴露的属性

| 属性名  | 说明                                          | 类型    | 可选值           | 默认值 |
| ------- | --------------------------------------------- | ------- | ---------------- | ------ |
| kbType  | 键盘类型。                                    | String  | 'city'｜'letter' | 'city' |
| display | 弹窗模式（popup为true时）下，键盘的展示状态。 | Boolean | true ｜ false    | false  |

<br>

