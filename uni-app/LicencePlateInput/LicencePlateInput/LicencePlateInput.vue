<!-- 车牌输入框 -->
<template>
	<view
		class="licence_plate_input"
		:style="{
			'--activeBorderColor': activeBorderColor,
			'--fontSize': cardStyle.width
		}"
	>
		<view
			v-for="(item, index) in plateNoArr"
			:class="[
				'input_x',
				{
					'active': activeIndex == index
				}
			]"
			@click="selected(index)"
			:style="cardStyle"
		>
			{{ item }}
		</view>

		<!-- +号 -->
		<view
			class="input_x"
			:style="cardStyle"
			v-show="plateNoArr.length == 7"
			@click="addDigit"
		>+</view>
	</view>
</template>

<script>
export default {
	name:"LicencePlateInput"
}
</script>

<script setup>
import { ref, computed, onMounted } from 'vue'

const props = defineProps({
	// 车牌号码
	'modelValue': {
		type: String,
		default: ''
	},
	// 车牌方块背景色
	'cardBackgroundColor': {
		type: String,
		default: '#eee'
	},
	// 字体颜色
	'textColor': {
		type: String,
		default: '#333'
	},
	// 激活项边框颜色
	'activeBorderColor': {
		type: String,
		default: '#1989fa'
	},
	'cardSize': {
		type: [Number, String],
		default: '80rpx'
	}
})

const emits = defineEmits([
	'update:modelValue', // 绑定的车牌号码
	'onInput', // 触发输入
	'onDelete', // 触发删除
	'focus' // 聚焦
])

// 样式
const cardStyle = computed(() => {
	let style = {
		'backgroundColor': props.cardBackgroundColor || '#eee',
		'color': props.textColor || '#333'
	}
	
	let size = '80rpx'
	switch(typeof(props.cardSize)) {
		case 'string':
			if (isNaN(props.cardSize)) {
				size = props.cardSize
			} else {
				size = props.cardSize + 'rpx'
			}
			break
		case 'number':
			size = props.cardSize + 'rpx'
			break
	}
	
	style.width = size
	style.height = size
	
	return style
})



// 当前激活项（-1为未选中任何项，在此状态下无法做输入和删除等操作）
const activeIndex = ref(-1)

/**
 * 手动选中项
 * @param {number} index
 */
function selected(index) {
	activeIndex.value = index
	emits('focus') // 告诉父组件，车牌输入框已经聚焦了
}

// 车牌号码
const plateNoArr = ref(Array(7).fill(''))

/**
 * 车牌号码加一位（新能源）
 */
function addDigit() {
	if (plateNoArr.value.length == 7) {
		plateNoArr.value.push('')
		selected(7)
	}
}

/**
 * 初始化车牌号码
 */
function init() {
	if (typeof props.modelValue == 'string') {
		for (let i = 0; i < 8; i++) {
			if (i == 7 && !props.modelValue[7]) {
				continue
			}
			plateNoArr.value[i] = props.modelValue[i] || ''
		}
	}
}

/**
 * 更新车牌
 */
function updatePlate() {
	emits('update:modelValue', plateNoArr.value.join(''))
}

/**
 * 外界输入的内容
 * 1. 将内容插入当前光标位置
 * 2. 将光标往后移动一位
 * @param {string} value
 */
function onInput(data) {
	if (activeIndex.value < plateNoArr.value.length && activeIndex.value >= 0) {
		plateNoArr.value[activeIndex.value] = data

		if (activeIndex.value < plateNoArr.value.length - 1) {
			activeIndex.value++
		}
	}
	
	updatePlate()
}

/**
 * 删除操作
 * 情况1. 如果光标位置有内容: 删除内容，光标位置保持不变
 * 情况2. 如果光标位置没内容: 往前移一位再删除内容
 * 情况3. 如果光标index为0: 删除第一个内容，光标不动
 */
function onDelete() {
	// 光标位置
	let index = activeIndex.value
	// 当前车牌内容
	let plate = plateNoArr.value
	
	if (index <= -1) {
		return
	}
	
	if (index > 0) {
		if (plate[index] == '') {
			// 情况2
			--index
		}
		// 情况1
		plate[index] = ''
	} else {
		// 情况3
		plate[0] = ''
	}
	
	plateNoArr.value = plate
	activeIndex.value = index
	
	updatePlate()
}

/**
 * 获取车牌数据
 * @return {string} str 字符串类型的车牌
 * @return {string} strDot 带圆点的车牌
 * @return {array} arr 数组类型的车牌
 * @return {boolean} integrality 车牌是否完整
 */
function getData() {

	let plate = plateNoArr.value

	let str = plate.join('')
	
	let strDot = ''
	
	let arr = []
	
	let integrality = true
	
	for (let i = 0; i < plate.length; i++) {
		// 用户有可能点击了加号（新能源）但没输入最后一位内容
		// 此时判断最后一位是否有内容，没有内容就切掉
		if (i == 7 && plate[i] == '') {
			continue
		}
		
		// 处理 strDot 的值
		strDot += plate[i]
		if (i == 1) {
			strDot += '•'
		}

		// 处理 arr 的值
		arr.push(plate[i])
		
		// 判断车牌完整性
		if (plate[i] == '') {
			integrality = false
		}
	}

	return {
		str,
		strDot,
		arr,
		integrality
	}
}

onMounted(() => {
	init()
})

defineExpose({
	activeIndex,
	onDelete,
	onInput,
	getData
})
</script>

<style lang="scss">
.licence_plate_input {
	width: 100%;
	display: flex;
	justify-content: space-between;

	.input_x {
		display: flex;
		justify-content: center;
		align-items: center;
		box-sizing: border-box;
		font-size: calc(var(--fontSize) * 0.48);
		border-radius: 8rpx;

		&.active {
			border: 1px solid var(--activeBorderColor);
		}
	}
}
</style>