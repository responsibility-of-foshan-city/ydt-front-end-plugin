# 车牌输入框 LicencePlateInput

- author: 杨正
- amend: -



## 平台兼容性

| Vue2 | Vue3 |
| :--: | :--: |
|  ×   |  √   |

| App  | 快应用 | 微信小程序 | 支付宝小程序 | 百度小程序 | 字节小程序 | QQ小程序 |
| :--: | :----: | :--------: | :----------: | :--------: | :--------: | :------: |
|  √   |   -    |     √      |      √       |     -      |     -      |    -     |

| 钉钉小程序 | 快手小程序 | 飞书小程序 | 京东小程序 |
| :--------: | :--------: | :--------: | :--------: |
|     -      |     -      |     -      |     -      |

| H5-Safari | Android Browser | 微信浏览器(Android) | QQ浏览器(Android) | Chrome |  IE  | Edge | Firefox | PC-Safari |
| :-------: | :-------------: | :-----------------: | :---------------: | :----: | :--: | :--: | :-----: | :-------: |
|     -     |        -        |          -          |         -         |   √    |  -   |  -   |    √    |     -     |

<br>

## 组件介绍

车牌输入框。

效果如下图所示。

![01](docData/01.gif)

<br>

功能：

- 支持7-8位车牌输入。
- 返回车牌号码（有字符串和数组两种类型）。
- 返回输出的车牌号码是否完整。
- 每输入一个字，光标就会往后移动一位。
- 支持删除功能。
- 容器宽度可以在 `LicencePlateInput` 外套一层 `view` 来控制。

<br>

建议：

最好配合 **LicencePlateKB** 组件一起使用

<br>

## 例子

### 基础用法

```html
<template>
  
  <!-- 车牌输入框 -->
  <LicencePlateInput
    v-model="plateNo"
    ref="LicencePlateInputRef"
    @focus="focus"
  />

  <!-- 车牌键盘 -->
  <LicencePlateKB
    ref="licencePlateKBRef"
		@onInput="onInput"
		@onDelete="onDelete"
		@onHide="onHide"
		v-show="showKB"
  />
</template>

<script setup>
import { ref } from 'vue'

// 车牌键盘
const licencePlateKBRef = ref(null)
// 车牌输入框
const LicencePlateInputRef = ref(null)

// 车牌
const plateNo = ref('')
  
// 输入内容
function onInput(data) {
	LicencePlateInputRef.value.onInput(data)
}

// 删除内容
function onDelete() {
	LicencePlateInputRef.value.onDelete()
}

// 键盘是否展示
const showKB = ref(false)

// 输入框聚焦时弹起车牌键盘
function focus() {
	showKB.value = true
}

// 隐藏键盘
function onHide() {
	showKB.value = false
}
</script>
```

<br>

<br>

## 属性

| 属性名 | 说明                                                  | 类型    | 可选值             | 默认值 |
| ------ | ----------------------------------------------------- | ------- | ------------------ | ------ |
| modelValue（v-model） | 车牌号码。 | String | - | '' |
| cardBackgroundColor | 车牌方块背景色。 | String | -     | '#eee' |
| textColor | 字体颜色。 | String | - | '#333' |
| activeBorderColor | 激活项边框颜色。 | String | - | '#1989fa' |
| cardSize | 输入框方块尺寸。值为数值型时会自动在末尾加上 'rpx' | Number, String | - | '80rpx' |

<br>

## 事件

| 事件名 | 说明   | 参数 | 返回值 |
| ------ | ------ | ---- | ------ |
| @focus | 聚焦。 | -    | -      |

<br>

## 方法

| 方法名   | 说明                                                         | 参数         | 返回值                          |
| -------- | ------------------------------------------------------------ | ------------ | ------------------------------- |
| onInput  | 输入一个字符。                                               | String: data | -                               |
| onDelete | 删除一个字符。                                               | -            | -                               |
| getData  | 获取当前输入的车牌数据，返回值有字符串类型车牌号码、加了点的字符串车牌号码、数组类型的车牌号码、车牌是否完整。 | -            | {str, strDot, arr, integrality} |

<br>

## 对外暴露的属性

| 属性名      | 说明           | 类型   | 可选值 | 默认值 |
| ----------- | -------------- | ------ | ------ | ------ |
| activeIndex | 当前光标位置。 | Number | -      | -1     |

<br>

