# Loading按钮 LoadingBtn1

- author: 杨正
- amend: -



## 平台兼容性

| Vue2 | Vue3 |
| :--: | :--: |
|  ×   |  √   |

| App  | 快应用 | 微信小程序 | 支付宝小程序 | 百度小程序 | 字节小程序 | QQ小程序 |
| :--: | :----: | :--------: | :----------: | :--------: | :--------: | :------: |
|  √   |   -    |     √      |      √       |     -      |     -      |    -     |

| 钉钉小程序 | 快手小程序 | 飞书小程序 | 京东小程序 |
| :--------: | :--------: | :--------: | :--------: |
|     -      |     -      |     -      |     -      |

| H5-Safari | Android Browser | 微信浏览器(Android) | QQ浏览器(Android) | Chrome |  IE  | Edge | Firefox | PC-Safari |
| :-------: | :-------------: | :-----------------: | :---------------: | :----: | :--: | :--: | :-----: | :-------: |
|     -     |        -        |          -          |         -         |   √    |  -   |  -   |    √    |     -     |

<br>

## 组件介绍

Loading按钮。

![01](./docData/01.gif)

<br>

![02](docData/02.gif)

<br>

## 例子

### 使用Loading按钮

```html
<LoadingBtn1
	@click="testClick"
	ref="LoadingBtnRef"
	text="确定"
>
</LoadingBtn1>

<script setup>
import { ref } from 'vue'

// 按钮元素
const LoadingBtnRef = ref(null)


function testClick() {
  // 点击后开始loading效果
	LoadingBtnRef.value.beginLoading()
  
  // 模拟http请求事件，2秒后返回结果
	setTimeout(() => {

    // 成功回调，执行success()
		// LoadingBtnRef.value.success('成功成功')

		// 失败回调，执行fail()
		LoadingBtnRef.value.fail('失败了')
	}, 2000)
}
</script>
```

<br>

## 属性

| 属性名 | 说明                                                  | 类型    | 可选值             | 默认值 |
| ------ | ----------------------------------------------------- | ------- | ------------------ | ------ |
| backgroundColor | 按钮背景色。 | String | 颜色值 | '#1989fa' |
| successBackground | 成功状态背景色 | String | 颜色值 | '#42b983' |
| failBackground | 失败状态背景色 | String | 颜色值 | '#e64340' |
| textColor | 文字颜色。 | String | 颜色值 | '#fff' |
| successTextColor | 成功状态文字颜色 | String | 颜色值 | '#fff' |
| failTextColor | 失败状态文本颜色 | String | 颜色值 | '#fff' |
| fontSize | 字号。值为数值型时会自动在末尾加上 'rpx'。 | String, Number | - | '32rpx' |
| height | 按钮高度。值为数值型时会自动在末尾加上 'rpx'。 | String, Number | - | '80rpx' |
| width | 按钮宽度 | String, Number | - | '200rpx' |
| resultWidth | 展示结果时按钮的宽度 | String, Number | - | '400rpx' |
| resultDuration | 结果展示的时长（毫秒） | Number | - | 2000 |
| text | 按钮默认文本 | String | - | '' |

<br>

# 方法

| 方法名          | 参数                    | 说明                |
| --------------- | ----------------------- | ------------------- |
| beginLoading()  | -                       | 开始进入loading状态 |
| finishLoading() | -                       | 结束loading状态     |
| success(msg)    | msg: 成功时要显示的文本 | 成功                |
| fail(msg)       | msg: 失败时要显示的文本 | 失败                |
| init()          | -                       | 恢复初始化的状态    |

<br>

# 插槽（不推荐）

按钮文本支持使用插槽的方式，但没特殊情况不推荐使用插槽，因为需要自行根据状态设置按钮文本。

```vue
<template>
	<view class="content">
		<LoadingBtn1
			@click="testClick"
			ref="LoadingBtnRef"
			:resultDuration="resultDuration"
		>
			<view class="">
				{{ btnText }}
			</view>
		</LoadingBtn1>
	</view>
</template>

<script setup>
import { ref } from 'vue'

// 按钮文本
const btnText = ref('确定')
// 按钮元素
const LoadingBtnRef = ref(null)
// 请求成功或者失败后按钮要显示多少秒（默认2000）
const resultDuration = ref(3000)


// 点击按钮
function testClick() {
	// 点击后开始loading效果
	LoadingBtnRef.value.beginLoading()

	// 模拟http请求事件，2秒后返回结果
	setTimeout(() => {

		// 成功回调，执行success()
		// LoadingBtnRef.value.success()
		// 失败回调，执行fail()
		LoadingBtnRef.value.fail()
		
		// 成功或者失败后，修改按钮文本
		btnText.value = "失败"
		
		setTimeout(() => {
			// 按钮回复默认状态后要显示的文本
			btnText.value = "确定"
		}, resultDuration.value)
	}, 2000)
}
</script>
```

<br>

如果不想传入 `resultDuration`，又想使用文本插槽的方式，可以通过 `LoadingBtnRef.value.resultDuration` 获取默认的时长。

```vue
<template>
	<view class="content">
		<LoadingBtn1
			@click="testClick"
			ref="LoadingBtnRef"
		>
			<view class="">
				{{ btnText }}
			</view>
		</LoadingBtn1>
	</view>
</template>

<script setup>
import { ref } from 'vue'

const btnText = ref('确定')
const LoadingBtnRef = ref(null)


function testClick() {
	LoadingBtnRef.value.beginLoading()
	setTimeout(() => {
		LoadingBtnRef.value.success()
		btnText.value = "数据获取成功"
		
		setTimeout(() => {
			btnText.value = "确定"
		}, LoadingBtnRef.value.resultDuration) // 注意这里！！！！！！！！！
	}, 2000)
}
</script>
```

