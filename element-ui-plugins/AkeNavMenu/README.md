# 无限级导航栏



## 介绍

> 基于 Element UI 的 `el-menu`、`el-submenu`、`el-menu-item` 组件。
>
> [点击查看 `Element UI` 导航菜单文档](https://element.eleme.io/#/zh-CN/component/menu)

<br>

无限级导航栏支持 `el-menu` 的所有属性和方法。

`el-submenu` 和 `el-menu-item` 属性需要在数据中配置。

![01.png](./docData/01.png)

<br>

## 引入

1. 将 `/AkeNavMenu` 复制到项目的 `/components` 里。
2. 在组件或页面中引入 `import AkeNavMenu from '@/components/AkeNavMenu'`

<br>

`/AkeNavMenu` 目录下包含 `index.vue` 和 `AkeNavLi.vue` 两个文件。

<br>

## 使用方法

```html
<template>
  <div class="page">
    <button @click="toggle">开关</button>
    <AkeNavMenu
      :data="navData"
      :collapse="isCollapse"
      default-active="a1-1"
      menuWidth="300"
      @open="open"
      @close="close"
      show-timeout="1000"
      ref="AkeNavMenuRef"
      router
    />
  </div>
</template>

<script>
import AkeNavMenu from '@/components/AkeNavMenu'
export default {
  components: {
    AkeNavMenu
  },
  data() {
    return {
      isCollapse: false,
      navData: [
        {
          id: 'a1',
          label: 'a1',
          icon: 'el-icon-location',
          children: [
            {
              id: 'a1-1',
              label: 'a1-1',
              icon: 'el-icon-platform-eleme',
              disabled: true, // 禁用该项
            },
            {
              id: 'a1-2',
              label: 'a1-2',
              icon: 'el-icon-platform-eleme',
              children: [
                {
                  id: 'a1-2-1',
                  label: 'a1-2-1',
                  icon: 'el-icon-platform-eleme',
                }
              ]
            }
          ]
        },
        {
          id: 'b1',
          label: 'b1',
          router: {...} // 配置路由
        }
      ]
    }
  },
  methods: {
    toggle() {
      this.isCollapse = !this.isCollapse
    },
    open(data) {
      console.log('open', data)
    },
    close(data) {
      console.log('close', data)
    },
  }
}
</script>
```

<br>

## 属性

| 属性名    | 说明                                                         | 类型            | 可选值                         | 默认值 |
| --------- | ------------------------------------------------------------ | --------------- | ------------------------------ | ------ |
| menuWidth | 导航栏展开时的宽度                                           | string / number | 100、'100px'、'10em'、'80%' 等 | 200    |
| 其他      | 其他属性和方法参照 [`Element UI` 导航菜单文档](https://element.eleme.io/#/zh-CN/component/menu) | -               | -                              | -      |

<br>

## 事件

| 事件名 | 说明                                                         | 参数 |
| ------ | ------------------------------------------------------------ | ---- |
| 其他   | 其他属性和方法参照 [`Element UI` 导航菜单文档](https://element.eleme.io/#/zh-CN/component/menu) | -    |

<br>

# 版本日志

## v1.0.0

- 导航宽度配置
- 无限级下钻
- 支持 `el-menu` 的属性和方法
