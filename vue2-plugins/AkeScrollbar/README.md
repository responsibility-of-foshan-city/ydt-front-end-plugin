# 虚拟滚动条



## 介绍

虚拟滚动条是一个容器组件。

仿照了 `Element Plus` 滚动条的样式。当鼠标移入容器时才会显示滚动条，鼠标移出后滚动条自动隐藏。

![01](docData/01.png)

<br>

## 引入

1. 将 `/AkeScrollbar` 复制到项目的 `/components` 里。
2. 在组件或页面中引入 `import AkeScrollbar from '@/components/AkeScrollbar'`

<br>

`/AkeScrollbar` 目录下包含 `index.vue` 文件。

<br>

## 使用方法

```html
<div class="page">
  <AkeScrollbar height="100px" :width="100" thumbColor="red">
    <div class="box">
      <div v-for="item in 300">
        {{ item }} 虚拟滚动条
      </div>
    </div>
  </AkeScrollbar>
</div>

<script>
import AkeScrollbar from '@/components/AkeScrollbar/AkeScrollbar.vue'
export default {
  components: {
    AkeScrollbar
  }
}
</script>
```

<br>

## 属性

| 属性名     | 说明                                      | 类型            | 可选值                             | 默认值 |
| ---------- | ----------------------------------------- | --------------- | ---------------------------------- | ------ |
| width      | 容器宽度，数值型的值会自动在末尾加上 `px` | string / number | 100、'100px'、'10em'、'80%' 等     | '100%' |
| height     | 容器高度，数值型的值会自动在末尾加上 `px` | string / number | 100、'100px'、'10em'、'80%' 等     | '100%' |
| overflowX  | 是否显示水平滚动条                        | boolean         | true / false                       | true   |
| overflowY  | 是否显示垂直滚动条                        | boolean         | true / false                       | true   |
| thumbColor | 滑块颜色。不需要设置颜色透明通道          | string          | hsl / hsv / hex / rgb / 颜色关键字 | '#000' |

<br>

## 插槽

| 插槽名称 | 说明           |
| -------- | -------------- |
| default  | 自定义默认内容 |

<br>

# 版本日志

## v1.0.0

- 自动显示/隐藏滚动条。
- 通过 `width`、`height` 设置容器宽高。
- 通过 `overflowX` 和 `overflowY` 控制滚动条是否显示。



## v1.0.1

- 新增滑块颜色设置
