# 页头



## 介绍

支持网站logo、网站名称配置。

支持页头背景色、文本颜色、虚拟滚动条滑块颜色配置。

![01](docData/01.png)

<br>

## 引入

1. 将 `/AkePageHeader` 复制到项目的 `/components` 里。
2. 在组件或页面中引入 `import AkePageHeader from '@/components/AkePageHeader'`

<br>

`/AkePageHeader` 目录下包含 `index.vue` 文件。

<br>

## 使用方法

```html
<div class="home">
    <AkePageHeader
      :logoSrc="logo"
      title="平台名称"
      @enableFlag="enableFlag"
    >

      <!-- 中间的导航菜单 -->
      <template #navList>
        <div class="nav_list">
          <div
            class="nav_li"
            v-for="item in 100"
            @click="skip(item)"
          >{{ item }}</div>
        </div>
      </template>

      <!-- 右侧的信息面板 -->
      <template #rightMenu>
        <div
          class="right_menu"
        >
          <div class="right_menu_li">主题</div>
          <div class="right_menu_li">下载</div>
          <div class="right_menu_li">admin</div>
        </div>
      </template>
    </AkePageHeader>
  </div>
</template>

<script>
import AkePageHeader from '@/components/AkePageHeader/AkePageHeader.vue'
import logo from '@/assets/logo.png'

export default {
  components: {
    AkePageHeader
  },
  data() {
    return {
      logo
    }
  },
  methods: {
    skip(data) {
      console.log(data)
    },
    enableFlag() {
      console.log('enableFlag')
    }
  }
}
</script>
```

<br>

## 属性

| 属性名          | 说明                                      | 类型            | 可选值                             | 默认值    |
| --------------- | ----------------------------------------- | --------------- | ---------------------------------- | --------- |
| height          | 页头高度。数值型的值会自动在末尾加上 `px` | string / number | 100、'100px'、'10em'、'80%' 等     | 56        |
| backgroundColor | 背景色                                    | string          | hsl / hsv / hex / rgb / 颜色关键字 | '#1b223d' |
| textColor       | 文本颜色                                  | string          | hsl / hsv / hex / rgb / 颜色关键字 | '#fff'    |
| logoSrc         | logo图片地址                              | string          | -                                  | -         |
| title           | 网站名称                                  | string          | -                                  | -         |
| thumbColor      | 滑块颜色。不需要设置颜色透明通道          | string          | hsl / hsv / hex / rgb / 颜色关键字 | '#fff'    |

<br>

## 事件

| 事件名     | 说明                         | 参数 |
| ---------- | ---------------------------- | ---- |
| enableFlag | 点击logo或者标题时触发的事件 | -    |

<br>

## 插槽

| 插槽名称  | 说明         |
| --------- | ------------ |
| navList   | 中间的导航栏 |
| rightMenu | 右侧信息面板 |

<br>

# 版本日志

## v1.0.0

- 页头高度配置
- 页头背景色配置
- 页头文本颜色配置
- logo地址配置
- 网站名称配置
- 滑块颜色配置
- 导航插槽
- 右侧面板插槽
- 点击logo事件
